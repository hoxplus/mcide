﻿using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Rendering;
using IDE.Utils;
using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace IDE
{
    class CodeLineNumberMargin : LineNumberMargin
    {
        protected override void OnRender(DrawingContext drawingContext)
        {
            TextView textView = this.TextView;
            Size renderSize = this.RenderSize;
            if (textView != null && textView.VisualLinesValid)
            {
                var foreground = (Brush)GetValue(Control.ForegroundProperty);
                int codeLineNumber = getCodeLineNumber(textView.VisualLines[0].FirstDocumentLine, textView.Document);
                foreach (VisualLine line in textView.VisualLines)
                {
                    int startOffset = line.FirstDocumentLine.Offset;
                    int endOffset = line.LastDocumentLine.EndOffset;  
                    String lineText = line.Document.GetText(startOffset, endOffset - startOffset);

                    if (lineText.isCodeLine())
                    {
                        FormattedText lineNumberText = CreateFormattedText(this, "0x" + codeLineNumber.ToString("x2"), typeface, emSize, foreground);
                        double y = line.GetTextLineVisualYPosition(line.TextLines[0], VisualYPosition.TextTop);
                        drawingContext.DrawText(lineNumberText, new Point(renderSize.Width - lineNumberText.Width, y - textView.VerticalOffset));
                        codeLineNumber++;
                    }
                }
            }
        }

        private int getCodeLineNumber(DocumentLine line, TextDocument doc)
        {
            int count = 0;
            foreach (var docLine in doc.Lines)
            {
                var startOffset = docLine.Offset;
                var endOffset = docLine.EndOffset;
                String lineText = doc.GetText(startOffset, endOffset - startOffset);
                if (docLine == line) return count;
                if (lineText.isCodeLine()) count++;
            }
            return count;
        }

        protected override void OnDocumentChanged(TextDocument oldDocument, TextDocument newDocument)
        {
            if (oldDocument != null)
            {
                PropertyChangedEventManager.RemoveListener(oldDocument, this, "LineCount");
            }
            base.OnDocumentChanged(oldDocument, newDocument);
            if (newDocument != null)
            {
                PropertyChangedEventManager.AddListener(newDocument, this, "LineCount");
            }
            OnDocumentLineCountChanged();
        }

        protected override bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(PropertyChangedEventManager))
            {
                OnDocumentLineCountChanged();
                return true;
            }
            return false;
        }

        private void OnDocumentLineCountChanged()
        {
            int documentLineCount = Document != null ? Document.LineCount : 1;
            int newLength = documentLineCount.ToString(CultureInfo.CurrentCulture).Length;

            // The margin looks too small when there is only one digit, so always reserve space for
            // at least four digits
            if (newLength < 4)
                newLength = 4;

            if (newLength != maxLineNumberLength)
            {
                maxLineNumberLength = newLength;
                InvalidateMeasure();
            }
        }

        private static FormattedText CreateFormattedText(FrameworkElement element, string text, Typeface typeface, double? emSize, Brush foreground)
        {
            return new FormattedText(
                text,
                CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                typeface,
                emSize.Value,
                foreground,
                null,
                TextOptions.GetTextFormattingMode(element)
            );
        }
    }
}
