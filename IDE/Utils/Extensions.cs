﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace IDE.Utils
{
    static class Extensions
    {
        public static int CoerceValue(this int value, int minimum, int maximum)
        {
            return Math.Max(Math.Min(value, maximum), minimum);


        }

        public static IEnumerable<int> GetAllIndexes(this string source, string matchString)
        {
            foreach (Match match in Regex.Matches(source, matchString))
            {
                yield return match.Index;
            }
        }

        public static IEnumerable<string> GetAllMatches(this string source, string matchString)
        {
            foreach (Match match in Regex.Matches(source, matchString))
            {
                yield return match.Value;
            }
        }

        public static bool isCodeLine(this string text)
        {
            return !text.StartsWith("#") && !String.IsNullOrEmpty(text.Trim(' '));
        }
    }
}
