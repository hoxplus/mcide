﻿using ICSharpCode.AvalonEdit.CodeCompletion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using System.Windows.Controls;
using ICSharpCode.AvalonEdit.Snippets;

namespace IDE.ViewModel.Texteditor
{
    class AutoComplitionData : ICompletionData
    {
        public AutoComplitionData(String text)
        {
            this.Text = text;
        }

        public System.Windows.Media.ImageSource Image
        {
            get { return null; }
        }

        public string Text { get; private set; }

        public object Content
        {
            get
            {
                return Text;
            }
        }

        public object Description
        {
            get { return "Description for " + this.Text; }
        }

        public double Priority { get { return 0; } }

        public void Complete(TextArea textArea, ISegment completionSegment, EventArgs insertionRequestEventArgs)
        {
            textArea.Document.Replace(completionSegment, this.Text + " ");
            Snippet snippet = new Snippet
            {
                Elements = {
                    new SnippetTextElement { Text = "0x" },
                    new SnippetReplaceableTextElement { Text = "00" },
                    new SnippetTextElement { Text = " " },
                    new SnippetCaretElement()
                }
            };
            snippet.Insert(textArea);
        }
    }
}
