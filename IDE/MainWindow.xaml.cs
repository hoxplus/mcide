﻿using ICSharpCode.AvalonEdit.CodeCompletion;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Highlighting;
using IDE.JMP;
using IDE.ViewModel.Texteditor;
using MicroCode.Compiler;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;

namespace IDE
{
    /// <summary>
    /// Interaktionslogik für MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static String HighlightingFileName = "IDE.CodeHighlighting.xshd";
        private static String HighlightingName = "Microcode Highlighting";
        private static String[] FileExtensions = new String[] { ".mc" };

        private JumpManager foldingManager;
        private JumpStrategie foldingStrat;
        private CompletionWindow completionWindow;

        Parser parser;

        public MainWindow()
        {
            InitializeComponent();

            IHighlightingDefinition customHighlighting = loadCustomHighlighting();
            HighlightingManager.Instance.RegisterHighlighting(HighlightingName, FileExtensions, customHighlighting);
            textEditor.SyntaxHighlighting = customHighlighting;

            textEditor.TextArea.TextEntering += textEditor_TextArea_TextEntering;
            textEditor.TextArea.TextEntered += textEditor_TextArea_TextEntered;

            textEditor.Document.Insert(0, "\n\n\n\naaaaaaaaa\nbbbbbbbb\ncccccccccc\nddddddddd\n\n\n\neeeeeeee\nfffffffff\nggggggggg\nhhhhhhhh\n");
            parser = new Parser(textEditor.Document);

            setUpCodeLineMargin();
            setUpDottedLineMargin();
            setUpJumpFoldingStrat();
        }

        private IHighlightingDefinition loadCustomHighlighting()
        {
            IHighlightingDefinition customHighlighting;
            using (Stream s = typeof(MainWindow).Assembly.GetManifestResourceStream(HighlightingFileName))
            {
                if (s == null)
                    throw new InvalidOperationException("Could not find embedded resource");
                using (XmlReader reader = new XmlTextReader(s))
                {
                    customHighlighting = ICSharpCode.AvalonEdit.Highlighting.Xshd.
                        HighlightingLoader.Load(reader, HighlightingManager.Instance);
                }
            }
            return customHighlighting;
        }

        private void setUpCodeLineMargin()
        {
            LineNumberMargin lineNumbers = new CodeLineNumberMargin();
            textEditor.TextArea.LeftMargins.Insert(0, lineNumbers);
            // ColorBinding
            Binding lineNumbersForeground = new Binding("LineNumbersForeground") { Source = textEditor };
            lineNumbers.SetBinding(Control.ForegroundProperty, lineNumbersForeground);
        }

        private void setUpDottedLineMargin()
        {
            Line line = (Line)DottedLineMargin.Create();
            textEditor.TextArea.LeftMargins.Insert(1, line);
            // ColorBinding
            Binding lineNumbersForeground = new Binding("LineNumbersForeground") { Source = textEditor };
            line.SetBinding(Line.StrokeProperty, lineNumbersForeground);
        }

        private void setUpJumpFoldingStrat()
        {
            foldingStrat = new JumpStrategie();
            foldingManager = JumpManager.Install(textEditor.TextArea);
            foldingStrat.UpdateFoldings(foldingManager, textEditor.Document);

            // start timer to regularly update Jump foldings
            DispatcherTimer foldingUpdateTimer = new DispatcherTimer();
            foldingUpdateTimer.Interval = TimeSpan.FromSeconds(0.2);
            foldingUpdateTimer.Tick += (s, e) => foldingStrat.UpdateFoldings(foldingManager, textEditor.Document);
            foldingUpdateTimer.Start();
        }

        private void textEditor_TextArea_TextEntered(object sender, TextCompositionEventArgs eventArgs)
        {
            if (Char.IsLetter(eventArgs.Text[0]) && completionWindow == null)
            {
                // open code completion after the user has pressed a letter
                completionWindow = new CompletionWindow(textEditor.TextArea);
                completionWindow.StartOffset--;
                // provide AvalonEdit with the data:
                IList<ICompletionData> data = completionWindow.CompletionList.CompletionData;
                data.Add(new AutoComplitionData("ADD"));
                data.Add(new AutoComplitionData("ADD"));
                data.Add(new AutoComplitionData("ADD"));
                data.Add(new AutoComplitionData("ADD"));
                data.Add(new AutoComplitionData("ADD"));
                data.Add(new AutoComplitionData("AfD"));
                data.Add(new AutoComplitionData("ADD"));
                data.Add(new AutoComplitionData("JMP"));
                data.Add(new AutoComplitionData("NOT"));
                data.Add(new AutoComplitionData("tes"));
                completionWindow.CompletionList.SelectItem(eventArgs.Text);
                completionWindow.Show();
                completionWindow.Closed += (s, e) => completionWindow = null;
            }
        }

        private void textEditor_TextArea_TextEntering(object sender, TextCompositionEventArgs e)
        {
            if (e.Text.Length > 0 && completionWindow != null)
            {
                if (!char.IsLetterOrDigit(e.Text[0]))
                {
                    // Whenever a non-letter is typed while the completion window is open,
                    // insert the currently selected element.
                    completionWindow.CompletionList.RequestInsertion(e);
                }
            }
            // do not set e.Handled=true - we still want to insert the character that was typed
        }
    }
}
