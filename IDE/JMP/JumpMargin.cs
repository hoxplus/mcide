﻿using ICSharpCode.AvalonEdit;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Rendering;
using ICSharpCode.AvalonEdit.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.TextFormatting;

namespace IDE.JMP
{
    class JumpMargin : AbstractMargin
    {
        public JumpManager JumpManager { get; set; }
        List<JumpMarginMarker> markers = new List<JumpMarginMarker>();

        internal const double SizeFactor = 4 / 3.0;

        #region Brushes
        /// <summary>
        /// FoldingMarkerBrush dependency property.
        /// </summary>
        public static readonly DependencyProperty FoldingMarkerBrushProperty =
            DependencyProperty.RegisterAttached("FoldingMarkerBrush", typeof(Brush), typeof(JumpMargin),
                                                new FrameworkPropertyMetadata(Brushes.Gray, FrameworkPropertyMetadataOptions.Inherits, OnUpdateBrushes));

        /// <summary>
        /// Gets/sets the Brush used for displaying the lines of folding markers.
        /// </summary>
        public Brush FoldingMarkerBrush
        {
            get { return (Brush)GetValue(FoldingMarkerBrushProperty); }
            set { SetValue(FoldingMarkerBrushProperty, value); }
        }

        /// <summary>
        /// FoldingMarkerBackgroundBrush dependency property.
        /// </summary>
        public static readonly DependencyProperty FoldingMarkerBackgroundBrushProperty =
            DependencyProperty.RegisterAttached("FoldingMarkerBackgroundBrush", typeof(Brush), typeof(JumpMargin),
                                                new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.Inherits, OnUpdateBrushes));

        /// <summary>
        /// Gets/sets the Brush used for displaying the background of folding markers.
        /// </summary>
        public Brush FoldingMarkerBackgroundBrush
        {
            get { return (Brush)GetValue(FoldingMarkerBackgroundBrushProperty); }
            set { SetValue(FoldingMarkerBackgroundBrushProperty, value); }
        }

        /// <summary>
        /// SelectedFoldingMarkerBrush dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedFoldingMarkerBrushProperty =
            DependencyProperty.RegisterAttached("SelectedFoldingMarkerBrush",
                                                typeof(Brush), typeof(JumpMargin),
                                                new FrameworkPropertyMetadata(Brushes.Black, FrameworkPropertyMetadataOptions.Inherits, OnUpdateBrushes));

        /// <summary>
        /// Gets/sets the Brush used for displaying the lines of selected folding markers.
        /// </summary>
        public Brush SelectedFoldingMarkerBrush
        {
            get { return (Brush)GetValue(SelectedFoldingMarkerBrushProperty); }
            set { SetValue(SelectedFoldingMarkerBrushProperty, value); }
        }

        /// <summary>
        /// SelectedFoldingMarkerBackgroundBrush dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedFoldingMarkerBackgroundBrushProperty =
            DependencyProperty.RegisterAttached("SelectedFoldingMarkerBackgroundBrush",
                                                typeof(Brush), typeof(JumpMargin),
                                                new FrameworkPropertyMetadata(Brushes.White, FrameworkPropertyMetadataOptions.Inherits, OnUpdateBrushes));

        /// <summary>
        /// Gets/sets the Brush used for displaying the background of selected folding markers.
        /// </summary>
        public Brush SelectedFoldingMarkerBackgroundBrush
        {
            get { return (Brush)GetValue(SelectedFoldingMarkerBackgroundBrushProperty); }
            set { SetValue(SelectedFoldingMarkerBackgroundBrushProperty, value); }
        }

        static void OnUpdateBrushes(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            JumpMargin m = null;
            if (d is JumpMargin)
                m = (JumpMargin)d;
            else if (d is TextEditor)
                m = ((TextEditor)d).TextArea.LeftMargins.FirstOrDefault(c => c is JumpMargin) as JumpMargin;
            if (m == null) return;
            if (e.Property.Name == FoldingMarkerBrushProperty.Name)
                m.foldingControlPen = MakeFrozenPen((Brush)e.NewValue);
            if (e.Property.Name == SelectedFoldingMarkerBrushProperty.Name)
                m.selectedFoldingControlPen = MakeFrozenPen((Brush)e.NewValue);
        }
        #endregion

        protected override int VisualChildrenCount { get { return markers.Count; } }
        protected override Visual GetVisualChild(int index) { return markers[index]; }

        Pen foldingControlPen = MakeFrozenPen((Brush)FoldingMarkerBrushProperty.DefaultMetadata.DefaultValue);
        Pen selectedFoldingControlPen = MakeFrozenPen((Brush)SelectedFoldingMarkerBrushProperty.DefaultMetadata.DefaultValue);

        static Pen MakeFrozenPen(Brush brush)
        {
            Pen pen = new Pen(brush, 1);
            pen.Freeze();
            return pen;
        }

        protected override void OnTextViewChanged(TextView oldTextView, TextView newTextView)
        {
            if (oldTextView != null)
            {
                oldTextView.VisualLinesChanged -= TextViewVisualLinesChanged;
            }
            base.OnTextViewChanged(oldTextView, newTextView);
            if (newTextView != null)
            {
                newTextView.VisualLinesChanged += TextViewVisualLinesChanged;
            }
            TextViewVisualLinesChanged(null, null);
        }

        void TextViewVisualLinesChanged(object sender, EventArgs e)
        {
            foreach (JumpMarginMarker m in markers)
            {
                RemoveVisualChild(m);
            }
            markers.Clear();
            InvalidateVisual();
            if (TextView != null && JumpManager != null && TextView.VisualLinesValid)
            {
                foreach (JumpSection fs in JumpManager.AllFoldings)
                {
                    if (fs == null)
                        continue;
                    var lastline = TextView.GetVisualLine(Document.GetLineByOffset(fs.EndOffset).LineNumber);
                    var firstline = TextView.GetVisualLine(Document.GetLineByOffset(fs.StartOffset).LineNumber);
                    if (lastline != null && !fs.Top)
                    {
                        JumpMarginMarker m = new JumpMarginMarker
                        {
                            VisualLine = lastline,
                            JumpSection = fs
                        };

                        markers.Add(m);
                        AddVisualChild(m);

                        m.IsMouseDirectlyOverChanged += delegate { InvalidateVisual(); };

                        InvalidateMeasure();
                        continue;
                    }
                    else if (firstline != null && fs.Top)
                    {
                        JumpMarginMarker m = new JumpMarginMarker
                        {
                            VisualLine = firstline,
                            JumpSection = fs
                        };

                        markers.Add(m);
                        AddVisualChild(m);

                        m.IsMouseDirectlyOverChanged += delegate { InvalidateVisual(); };

                        InvalidateMeasure();
                        continue;
                    }
                }
            }
        }

        protected override Size MeasureOverride(Size availableSize)
        {
            foreach (JumpMarginMarker m in markers)
            {
                m.Measure(availableSize);
            }
            double width = SizeFactor * (double)GetValue(TextBlock.FontSizeProperty);
            return new Size(PixelSnapHelpers.RoundToOdd(width, PixelSnapHelpers.GetPixelSize(this).Width), 0);
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            Size pixelSize = PixelSnapHelpers.GetPixelSize(this);
            foreach (JumpMarginMarker m in markers)
            {
                int visualColumn = m.VisualLine.GetVisualColumn(m.JumpSection.EndOffset - m.VisualLine.FirstDocumentLine.Offset);
                TextLine textLine = m.VisualLine.GetTextLine(visualColumn);
                double yPos = m.VisualLine.GetTextLineVisualYPosition(textLine, VisualYPosition.TextMiddle) - TextView.VerticalOffset;
                yPos -= m.DesiredSize.Height / 2;
                double xPos = (finalSize.Width - m.DesiredSize.Width) / 2;
                m.Arrange(new Rect(PixelSnapHelpers.Round(new Point(xPos, yPos), pixelSize), m.DesiredSize));
            }
            return base.ArrangeOverride(finalSize);
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (TextView == null || !TextView.VisualLinesValid) return;
            if (TextView.VisualLines.Count == 0 || JumpManager == null) return;

            var allTextLines = TextView.VisualLines.SelectMany(vl => vl.TextLines).ToList();
            Pen[] colors = new Pen[allTextLines.Count + 1];
            Pen[] endMarker = new Pen[allTextLines.Count];
            Pen[] arrow = new Pen[allTextLines.Count];

            CalculateFoldLinesForFoldingsActiveAtStart(allTextLines, colors, endMarker, arrow);
            CalculateFoldLinesForFoldingsActiveAtEnd(allTextLines, colors, endMarker, arrow);
            CalculateFoldLinesForMarkers(allTextLines, colors, endMarker, arrow);
            DrawFoldLines(drawingContext, colors, endMarker, arrow);

        }

        void DrawFoldLines(DrawingContext drawingContext, Pen[] colors, Pen[] endMarker, Pen[] arrow)
        {
            Size pixelSize = PixelSnapHelpers.GetPixelSize(this);
            double markerXPos = PixelSnapHelpers.PixelAlign(RenderSize.Width / 2, pixelSize.Width);
            double startY = 0;
            Pen currentPen = colors[0];
            int tlNumber = 0;
            foreach (VisualLine vl in TextView.VisualLines)
            {
                foreach (TextLine tl in vl.TextLines)
                {
                    if(arrow[tlNumber] != null)
                    {
                        double visualPos = GetVisualPos(vl, tl, pixelSize.Height);
                        drawingContext.DrawLine(arrow[tlNumber], new Point(markerXPos - pixelSize.Width / 2, visualPos), new Point(RenderSize.Width, visualPos));
                        drawingContext.DrawLine(arrow[tlNumber], new Point((markerXPos - pixelSize.Width / 2) * 1.5, visualPos - tl.Height / 4), new Point(RenderSize.Width, visualPos));
                        drawingContext.DrawLine(arrow[tlNumber], new Point((markerXPos - pixelSize.Width / 2) * 1.5, visualPos + tl.Height / 4), new Point(RenderSize.Width, visualPos));
                    }
                    if (endMarker[tlNumber] != null)
                    {
                        double visualPos = GetVisualPos(vl, tl, pixelSize.Height);
                        drawingContext.DrawLine(endMarker[tlNumber], new Point(markerXPos - pixelSize.Width / 2, visualPos), new Point(RenderSize.Width, visualPos));
                    }
                    if (colors[tlNumber + 1] != currentPen)
                    {
                        double visualPos = GetVisualPos(vl, tl, pixelSize.Height);
                        if (currentPen != null)
                        {
                            drawingContext.DrawLine(currentPen, new Point(markerXPos, startY + pixelSize.Height / 2), new Point(markerXPos, visualPos - pixelSize.Height / 2));
                        }
                        currentPen = colors[tlNumber + 1];
                        startY = visualPos;
                    }
                    tlNumber++;
                }
            }
            if (currentPen != null)
            {
                drawingContext.DrawLine(currentPen, new Point(markerXPos, startY + pixelSize.Height / 2), new Point(markerXPos, RenderSize.Height));
            }
        }

        double GetVisualPos(VisualLine vl, TextLine tl, double pixelHeight)
        {
            double pos = vl.GetTextLineVisualYPosition(tl, VisualYPosition.TextMiddle) - TextView.VerticalOffset;
            return PixelSnapHelpers.PixelAlign(pos, pixelHeight);
        }

        void CalculateFoldLinesForFoldingsActiveAtStart(List<TextLine> allTextLines, Pen[] colors, Pen[] endMarker, Pen[] arrow)
        {
            int viewStartOffset = TextView.VisualLines[0].FirstDocumentLine.Offset;
            int viewEndOffset = TextView.VisualLines.Last().LastDocumentLine.EndOffset;
            var foldings = JumpManager.GetFoldingsContaining(viewStartOffset);
            int maxEndOffset = 0;
            foreach (JumpSection fs in foldings)
            {
                int end = fs.EndOffset;
                if (end <= viewEndOffset)
                {
                    int textLineNr = GetTextLineIndexFromOffset(allTextLines, end);
                    if (textLineNr >= 0)
                    {
                        if(!fs.Top) endMarker[textLineNr] = foldingControlPen;
                        else arrow[textLineNr] = foldingControlPen;
                    }
                }
                if (end > maxEndOffset && fs.StartOffset < viewStartOffset)
                {
                    maxEndOffset = end;
                }
            }
            if (maxEndOffset > 0)
            {
                if (maxEndOffset > viewEndOffset)
                {
                    for (int i = 0; i < colors.Length; i++)
                    {
                        colors[i] = foldingControlPen;
                    }
                }
                else
                {
                    int maxTextLine = GetTextLineIndexFromOffset(allTextLines, maxEndOffset);
                    for (int i = 0; i <= maxTextLine; i++)
                    {
                        colors[i] = foldingControlPen;
                    }
                }
            }
        }
        void CalculateFoldLinesForFoldingsActiveAtEnd(List<TextLine> allTextLines, Pen[] colors, Pen[] endMarker, Pen[] arrow)
        {
            int viewStartOffset = TextView.VisualLines[0].FirstDocumentLine.Offset;
            int viewEndOffset = TextView.VisualLines.Last().LastDocumentLine.EndOffset;
            var foldings = JumpManager.GetFoldingsContaining(viewEndOffset);
            int maxStartOffset = Document.TextLength;
            foreach (JumpSection fs in foldings)
            {
                int start = fs.StartOffset;
                if (start >= viewStartOffset)
                {
                    int textLineNr = GetTextLineIndexFromOffset(allTextLines, start);
                    if (textLineNr >= 0)
                    {
                        if (fs.Top) endMarker[textLineNr] = foldingControlPen;
                        else arrow[textLineNr] = foldingControlPen;
                    }
                }
                if (start < maxStartOffset && fs.EndOffset > viewEndOffset)
                {
                    maxStartOffset = start;
                }
            }
            if (maxStartOffset < Document.TextLength)
            {
                if (maxStartOffset < viewStartOffset)
                {
                    for (int i = 0; i < colors.Length; i++)
                    {
                        colors[i] = foldingControlPen;
                    }
                }
                else
                {
                    int maxTextLine = GetTextLineIndexFromOffset(allTextLines, maxStartOffset);
                    for (int i = colors.Length - 1; i > maxTextLine; i--)
                    {
                        colors[i] = foldingControlPen;
                    }
                }
            }
        }
        void CalculateFoldLinesForMarkers(List<TextLine> allTextLines, Pen[] colors, Pen[] endMarker, Pen[] arrow)
        {
            foreach (JumpMarginMarker marker in markers)
            {
                int end = marker.JumpSection.EndOffset;
                int endTextLineNr = GetTextLineIndexFromOffset(allTextLines, end);
                if (endTextLineNr >= 0)
                {
                    if (!marker.JumpSection.Top)
                    {
                        if (marker.IsMouseDirectlyOver)
                            endMarker[endTextLineNr] = selectedFoldingControlPen;
                        else if (endMarker[endTextLineNr] == null)
                            endMarker[endTextLineNr] = foldingControlPen;
                    }
                    else
                    {
                        if (marker.IsMouseDirectlyOver)
                            arrow[endTextLineNr] = selectedFoldingControlPen;
                        else if (endMarker[endTextLineNr] == null)
                            arrow[endTextLineNr] = foldingControlPen;
                    }
                }
                int start = marker.JumpSection.StartOffset;
                int startTextLineNr = GetTextLineIndexFromOffset(allTextLines, start);
                if (startTextLineNr >= 0)
                {
                    if (!marker.JumpSection.Top)
                    {
                        if (marker.IsMouseDirectlyOver)
                            arrow[startTextLineNr] = selectedFoldingControlPen;
                        else if (endMarker[startTextLineNr] == null)
                            arrow[startTextLineNr] = foldingControlPen;
                    }
                    else
                    {
                        if (marker.IsMouseDirectlyOver)
                            endMarker[startTextLineNr] = selectedFoldingControlPen;
                        else if (endMarker[startTextLineNr] == null)
                            endMarker[startTextLineNr] = foldingControlPen;
                    }

                }
                startTextLineNr = GetTextLineIndexFromOffset(allTextLines, marker.JumpSection.StartOffset);
                if (startTextLineNr >= 0)
                {
                    for (int i = startTextLineNr + 1; i < colors.Length && i - 1 != endTextLineNr; i++)
                    {
                        if (marker.IsMouseDirectlyOver)
                            colors[i] = selectedFoldingControlPen;
                        else if (colors[i] == null)
                            colors[i] = foldingControlPen;
                    }
                }
            }
        }

        int GetTextLineIndexFromOffset(List<TextLine> textLines, int offset)
        {
            int lineNumber = TextView.Document.GetLineByOffset(offset).LineNumber;
            VisualLine vl = TextView.GetVisualLine(lineNumber);
            if (vl != null)
            {
                int relOffset = offset - vl.FirstDocumentLine.Offset;
                TextLine line = vl.GetTextLine(vl.GetVisualColumn(relOffset));
                return textLines.IndexOf(line);
            }
            return -1;
        }
    }
}
