﻿using ICSharpCode.AvalonEdit.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDE.JMP
{
    class JumpSection : TextSegment
    {
        readonly JumpManager manager;
        string title;
        bool top;

        public JumpSection(JumpManager mngr, int startOffset, int endOffset)
        {
            this.manager = mngr;
            this.StartOffset = startOffset;
            this.Length = endOffset - startOffset;
        }

        public object Tag { get; set; }

        public bool Top
        {
            get
            {
                return top;
            }
            set
            {
                if (top != value)
                {
                    top = value;
                    manager.Redraw(this);
                }
            }
        }

        public string Title
        {
            get
            {
                return title;
            }
            set
            {
                if (title != value)
                {
                    title = value;
                    manager.Redraw(this);
                }
            }
        }
        
        public string TextContent
        {
            get
            {
                return manager.document.GetText(StartOffset, EndOffset - StartOffset);
            }
        }
    }
}
