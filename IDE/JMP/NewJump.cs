﻿using ICSharpCode.AvalonEdit.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDE.JMP
{
    class NewJump : ISegment
    {
		public int StartOffset { get; set; }
        public int EndOffset { get; set; }

        public string Name { get; set; }   
        public bool Top { get; set; }
        
        public NewJump()
        {
        }
        
        public NewJump(int start, int end, bool top)
        {
            if (!(start <= end))
                throw new ArgumentException("'start' must be less than 'end'");
            this.StartOffset = start;
            this.EndOffset = end;
            this.Name = null;
            this.Top = top;
        }

        int ISegment.Offset
        {
            get { return this.StartOffset; }
        }

        int ISegment.Length
        {
            get { return this.EndOffset - this.StartOffset; }
        }
    }
}
