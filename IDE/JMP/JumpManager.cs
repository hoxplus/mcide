﻿using ICSharpCode.AvalonEdit.Document;
using ICSharpCode.AvalonEdit.Editing;
using ICSharpCode.AvalonEdit.Rendering;
using IDE.Utils;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace IDE.JMP
{
    class JumpManager : IWeakEventListener
    {
        internal readonly List<TextView> textViews = new List<TextView>();

        internal readonly TextDocument document;
        readonly TextSegmentCollection<JumpSection> jumps;

        #region Constructor
        public JumpManager(TextDocument document)
        {
            if (document == null)
                throw new ArgumentNullException("document");
            this.document = document;
            this.jumps = new TextSegmentCollection<JumpSection>();
            document.VerifyAccess();
            TextDocumentWeakEventManager.Changed.AddListener(document, this);
        }
        #endregion

        #region Weak
        public bool ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            if (managerType == typeof(TextDocumentWeakEventManager.Changed))
            {
                OnDocumentChanged((DocumentChangeEventArgs)e);
                return true;
            }
            return false;
        }

        bool IWeakEventListener.ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            return ReceiveWeakEvent(managerType, sender, e);
        }

        private void OnDocumentChanged(DocumentChangeEventArgs e)
        {
            jumps.UpdateOffsets(e);
            int newEndOffset = e.Offset + e.InsertionLength;
            // extend end offset to the end of the line (including delimiter)
            var endLine = document.GetLineByOffset(newEndOffset);
            newEndOffset = endLine.Offset + endLine.TotalLength;
            foreach (var affectedFolding in jumps.FindOverlappingSegments(e.Offset, newEndOffset - e.Offset))
            {
                if (affectedFolding.Length == 0)
                {
                    RemoveFolding(affectedFolding);
                }
            }
        }
        #endregion

        #region Manage TextViews
        internal void AddToTextView(TextView textView)
        {
            if (textView == null || textViews.Contains(textView))
                throw new ArgumentException();
            textViews.Add(textView); 
        }

        internal void RemoveFromTextView(TextView textView)
        {
            int pos = textViews.IndexOf(textView);
            if (pos < 0)
                throw new ArgumentException();
            textViews.RemoveAt(pos);
        }

        internal void Redraw()
        {
            foreach (TextView textView in textViews)
                textView.Redraw();
        }

        internal void Redraw(JumpSection js)
        {
            foreach (TextView textView in textViews)
                textView.Redraw(js);
        }
        #endregion

        #region Create / Remove / Clear
        public JumpSection CreateFolding(int startOffset, int endOffset)
        {
            if (startOffset >= endOffset)
                throw new ArgumentException("startOffset must be less than endOffset");
            if (startOffset < 0 || endOffset > document.TextLength)
                throw new ArgumentException("Folding must be within document boundary");
            JumpSection fs = new JumpSection(this, startOffset, endOffset);
            jumps.Add(fs);
            Redraw(fs);
            return fs;
        }

        public void RemoveFolding(JumpSection fs)
        {
            if (fs == null)
                throw new ArgumentNullException("fs");
            jumps.Remove(fs);
            Redraw(fs);
        }

        /// <summary>
        /// Removes all folding sections.
        /// </summary>
        public void Clear()
        {
            document.VerifyAccess();
            jumps.Clear();
            Redraw();
        }
        #endregion

        #region Get...Jumps 
		public IEnumerable<JumpSection> AllFoldings
        {
            get { return jumps; }
        }

        public int GetNextFoldedFoldingStart(int startOffset)
        {
            JumpSection js = jumps.FindFirstSegmentWithStartAfter(startOffset);
            while (js != null)
                js = jumps.GetNextSegment(js);
            return js != null ? js.StartOffset : -1;
        }

        public JumpSection GetNextFolding(int startOffset)
        {
            // TODO: returns the longest folding instead of any folding at the first position after startOffset
            return jumps.FindFirstSegmentWithStartAfter(startOffset);
        }

        public ReadOnlyCollection<JumpSection> GetFoldingsAt(int startOffset)
        {
            List<JumpSection> result = new List<JumpSection>();
            JumpSection fs = jumps.FindFirstSegmentWithStartAfter(startOffset);
            while (fs != null && fs.StartOffset == startOffset)
            {
                result.Add(fs);
                fs = jumps.GetNextSegment(fs);
            }
            return result.AsReadOnly();
        }

        public ReadOnlyCollection<JumpSection> GetFoldingsContaining(int offset)
        {
            return jumps.FindSegmentsContaining(offset);
        }
        #endregion

        #region UpdateFoldings
        
        public void UpdateFoldings(IEnumerable<NewJump> newJumps, int firstErrorOffset)
        {
            if (newJumps == null)
                throw new ArgumentNullException("newJumps");

            if (firstErrorOffset < 0)
                firstErrorOffset = int.MaxValue;

            var oldJumps = this.AllFoldings.ToArray();
            int oldJumpIndex = 0;
            int previousStartOffset = 0;
            // merge new foldings into old foldings so that sections keep being collapsed
            // both oldFoldings and newFoldings are sorted by start offset
            foreach (NewJump newJump in newJumps)
            {
                // ensure newFoldings are sorted correctly
                if (newJump.StartOffset < previousStartOffset)
                    throw new ArgumentException("newFoldings must be sorted by start offset");
                previousStartOffset = newJump.StartOffset;

                int startOffset = newJump.StartOffset.CoerceValue(0, document.TextLength);
                int endOffset = newJump.EndOffset.CoerceValue(0, document.TextLength);

                if (newJump.StartOffset == newJump.EndOffset)
                    continue; // ignore zero-length jumps

                // remove old jumps that were skipped
                while (oldJumpIndex < oldJumps.Length && newJump.StartOffset > oldJumps[oldJumpIndex].StartOffset)
                {
                    this.RemoveFolding(oldJumps[oldJumpIndex++]);
                }
                JumpSection section;
                // reuse current jump if its matching:
                if (oldJumpIndex < oldJumps.Length && newJump.StartOffset == oldJumps[oldJumpIndex].StartOffset)
                {
                    section = oldJumps[oldJumpIndex++];
                    section.Length = newJump.EndOffset - newJump.StartOffset;
                    section.Top = newJump.Top;
                }
                else
                {
                    // no matching current jump; create a new one:
                    section = this.CreateFolding(newJump.StartOffset, newJump.EndOffset);                    
                    section.Tag = newJump;
                    section.Top = newJump.Top;
                }
                section.Title = newJump.Name;
            }

            // remove all outstanding old foldings:
            while (oldJumpIndex < oldJumps.Length)
            {
                JumpSection oldSection = oldJumps[oldJumpIndex++];
                if (oldSection.StartOffset >= firstErrorOffset)
                    break;
                this.RemoveFolding(oldSection);
            }
        }
        #endregion

        #region Install        
        public static JumpManager Install(TextArea textArea)
        {
            if (textArea == null)
                throw new ArgumentNullException("textArea");
            return new JumpManagerInstallation(textArea);
        }

        public static void Uninstall(JumpManager manager)
        {
            if (manager == null)
                throw new ArgumentNullException("manager");
            JumpManagerInstallation installation = manager as JumpManagerInstallation;
            if (installation != null)
            {
                installation.Uninstall();
            }
            else
            {
                throw new ArgumentException("JumpManager was not created using JumpManager.Install");
            }
        }

        sealed class JumpManagerInstallation : JumpManager
        {
            TextArea textArea;
            JumpMargin margin;
            //FoldingElementGenerator generator;

            public JumpManagerInstallation(TextArea textArea) : base(textArea.Document)
            {
                this.textArea = textArea;
                margin = new JumpMargin() { JumpManager = this };
                margin.FoldingMarkerBrush = Brushes.Red;
                margin.SelectedFoldingMarkerBrush = Brushes.Green;
                //generator = new FoldingElementGenerator() { FoldingManager = this };
                textArea.LeftMargins.Insert(0, margin);
                textArea.TextView.Services.AddService(typeof(JumpManager), this);
                AddToTextView(textArea.TextView);
                // HACK: folding only works correctly when it has highest priority
                //textArea.TextView.ElementGenerators.Insert(0, generator);
                //textArea.Caret.PositionChanged += textArea_Caret_PositionChanged;
            }


            public void Uninstall()
            {
                Clear();
                if (textArea != null)
                {
                    //textArea.Caret.PositionChanged -= textArea_Caret_PositionChanged;
                    textArea.LeftMargins.Remove(margin);
                    //textArea.TextView.ElementGenerators.Remove(generator);
                    textArea.TextView.Services.RemoveService(typeof(JumpManager));
                    RemoveFromTextView(textArea.TextView);
                    margin = null;
                    //generator = null;
                    textArea = null;
                }
            }

            //void textArea_Caret_PositionChanged(object sender, EventArgs e)
            //{
            //    // Expand Foldings when Caret is moved into them.
            //    int caretOffset = textArea.Caret.Offset;
            //    foreach (FoldingSection s in GetFoldingsContaining(caretOffset))
            //    {
            //        if (s.IsFolded && s.StartOffset < caretOffset && caretOffset < s.EndOffset)
            //        {
            //            s.IsFolded = false;
            //        }
            //    }
            //}
        }
        #endregion
    }
}
