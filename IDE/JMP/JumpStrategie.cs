﻿using ICSharpCode.AvalonEdit.Document;
using IDE.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IDE.JMP
{
    class JumpStrategie
    {
        public char OpeningBrace { get; set; }
        public char ClosingBrace { get; set; }

        public JumpStrategie()
        {
            this.OpeningBrace = '[';
            this.ClosingBrace = ']';
        }

        public void UpdateFoldings(JumpManager manager, TextDocument document)
        {
            int firstErrorOffset;
            IEnumerable<NewJump> newFoldings = CreateNewFoldings(document, out firstErrorOffset);
            manager.UpdateFoldings(newFoldings, firstErrorOffset);
        }

        public IEnumerable<NewJump> CreateNewFoldings(TextDocument document, out int firstErrorOffset)
        {
            firstErrorOffset = -1;
            return CreateNewFoldings(document);
        }

        public IEnumerable<NewJump> CreateNewFoldings(TextDocument document)
        {
            string reges = @"((JMP)|(JMN)|(JME))\s+0(([xX][0-9a-fA-F]+)|([bB][0-1]+))";
            string regex = @"(0[xX][0-9a-fA-F]+)|(0[bB][0-1]+)";

            List<NewJump> newFoldings = new List<NewJump>();
            var indices = document.Text.GetAllIndexes(reges).ToList();
            var matches = document.Text.GetAllMatches(reges).ToList();
            for (int i = 0; i < matches.Count; i++)
            {
                var numbers = matches[i].GetAllMatches(regex).ToList();
                int firstOffset = indices[i];
                int lastOffset;
                if (numbers.Count > 0)
                    lastOffset = Convert.ToInt32(numbers[0], (numbers[0][1] == 'x' || numbers[0][1] == 'X') ? 16 : 2);
                else lastOffset = firstOffset + 1;
                if (lastOffset > 0 && lastOffset <= document.LineCount)
                    lastOffset = document.GetLineByNumber(lastOffset).Offset;
                else lastOffset = firstOffset + 1;
                newFoldings.Add(new NewJump(Math.Min(firstOffset, lastOffset), Math.Max(firstOffset, lastOffset), firstOffset < lastOffset));
            }
            newFoldings.Sort((a, b) => a.StartOffset.CompareTo(b.StartOffset));
            return newFoldings;
        }
    }
}
