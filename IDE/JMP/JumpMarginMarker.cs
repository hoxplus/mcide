﻿using ICSharpCode.AvalonEdit.Rendering;
using ICSharpCode.AvalonEdit.Utils;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
namespace IDE.JMP
{
    class JumpMarginMarker : UIElement
    {
        internal VisualLine VisualLine;
        internal JumpSection JumpSection;

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (!e.Handled)
            {

            }
        }

        const double MarginSizeFactor = 0.7;

        protected override Size MeasureCore(Size availableSize)
        {
            double size = MarginSizeFactor * JumpMargin.SizeFactor * (double)GetValue(TextBlock.FontSizeProperty);
            size = PixelSnapHelpers.RoundToOdd(size, PixelSnapHelpers.GetPixelSize(this).Width);
            return new Size(size, size);
        }

            

        protected override void OnRender(DrawingContext drawingContext)
        {
            JumpMargin margin = VisualParent as JumpMargin;
            Pen activePen = new Pen(margin.SelectedFoldingMarkerBrush, 1);
            Pen inactivePen = new Pen(margin.FoldingMarkerBrush, 1);
            activePen.StartLineCap = inactivePen.StartLineCap = PenLineCap.Square;
            activePen.EndLineCap = inactivePen.EndLineCap = PenLineCap.Square;
            Size pixelSize = PixelSnapHelpers.GetPixelSize(this);
            Rect rect = new Rect(pixelSize.Width / 2,
                                 pixelSize.Height / 2,
                                 this.RenderSize.Width - pixelSize.Width,
                                 this.RenderSize.Height - pixelSize.Height);
            drawingContext.DrawRectangle(
                IsMouseDirectlyOver ? margin.SelectedFoldingMarkerBackgroundBrush : margin.FoldingMarkerBackgroundBrush,
                IsMouseDirectlyOver ? activePen : inactivePen, rect);
            double middleX = rect.Left + rect.Width / 2;
            double middleY = rect.Top + rect.Height / 2;
            Point p1 = new Point(middleX, rect.Top + rect.Height / 5);
            Point p2 = new Point(rect.Left + rect.Width / 5, rect.Top + 4*(rect.Height / 5));
            Point p3 = new Point(rect.Left + 4*(rect.Width / 5), rect.Top + 4 * (rect.Height / 5));
            StreamGeometry streamGeometry = new StreamGeometry();
            using (StreamGeometryContext geometryContext = streamGeometry.Open())
            {
                geometryContext.BeginFigure(p1, true, true);
                PointCollection points = new PointCollection { p2, p3 };
                geometryContext.PolyLineTo(points, true, true);
            }
            if(JumpSection.Top) streamGeometry.Transform = new RotateTransform(180, middleX, middleY);
            streamGeometry.Freeze();
            drawingContext.DrawGeometry(IsMouseDirectlyOver ? activePen.Brush : inactivePen.Brush,
                                        IsMouseDirectlyOver ? activePen : inactivePen, streamGeometry);
        }

        protected override void OnIsMouseDirectlyOverChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnIsMouseDirectlyOverChanged(e);
            InvalidateVisual();
        }
    }
}
