﻿using ICSharpCode.AvalonEdit.Document;
using MicroCode.Utils;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace MicroCode.Compiler
{
    public class Parser
    {
        private static readonly string slCommentStart = "#";
        private static readonly Dictionary<string, string> cmdRegex;
        private static readonly Dictionary<string, CPUCommand.CommandKeyWord> cmdKeyWords;

        private Dictionary<DocumentLine, CodeLine> codeLines;
        private TextDocument document;
        private int insertLineOfSet = 0;
        private int removeLineOfSet = 0;

        static Parser()
        {
            String argumentRegex = "0(([xX][0-9a-fA-F]+)|([bB][0-1]+))";
            cmdRegex = new Dictionary<string, string>();
            cmdKeyWords = new Dictionary<string, CPUCommand.CommandKeyWord>();

            cmdRegex.Add("ADD", "ADD" + " " + argumentRegex + " ");
            cmdKeyWords.Add("ADD", CPUCommand.CommandKeyWord.ADD);
            cmdRegex.Add("JMP", "ADD" + " " + argumentRegex + " ");
            cmdKeyWords.Add("JMP", CPUCommand.CommandKeyWord.JMP);
            cmdRegex.Add("HLT", "HLT");
            cmdKeyWords.Add("HLT", CPUCommand.CommandKeyWord.HLT);
        }

        public Parser(TextDocument txtDocument)
        {
            document = txtDocument;
            document.Changing += documentChanging;
            document.Changed += documentChanged;
            IEnumerable<CodeLine> lines = getCodeLines(document.Lines);
            codeLines = lines.ToDictionary((cdl) => cdl.DocLine);
        }

        private void documentChanging(object sender, DocumentChangeEventArgs e)
        {
            insertLineOfSet = removeLineOfSet = 0;
            string oneLine = Regex.Replace(document.GetText(0, e.Offset), @"\t|\n|\r", "").Trim();
            if (string.IsNullOrEmpty(oneLine))
            {
                insertLineOfSet = e.InsertedText.Text.Count(chr => chr == '\n');
                removeLineOfSet = e.RemovedText.Text.Count(chr => chr == '\n');
            }
        }

        private void documentChanged(object sender, DocumentChangeEventArgs e)
        {
            updateLineNumbers(e.Offset);
            if (e.RemovalLength > 0) removeCodeLines(e.Offset);
            if (e.InsertionLength > 0) insertCodeLines(e.Offset, e.InsertionLength);
        }

        private void updateLineNumbers(int offset)
        {
            DocumentLine docLine = document.GetLineByOffset(offset);
            if (removeLineOfSet != 0)
            {
                CodeLine deletedLine = codeLines.Values.Where((cl) => cl.DocLine.IsDeleted).FirstOrDefault();
                if (deletedLine != null)
                {
                    codeLines.ChangeKey(deletedLine.DocLine, docLine);
                    deletedLine.DocLine = docLine;
                }
            }
            if (insertLineOfSet != 0)
            {                
                CodeLine zeroLine = codeLines.Values.Where((cl) => cl.DocLine == docLine).FirstOrDefault();
                if (zeroLine != null)
                {
                    docLine = document.GetLineByNumber(docLine.LineNumber + insertLineOfSet);
                    CodeLine updatedLine = new CodeLine(docLine, zeroLine.PreviouseLine, zeroLine.NextLine);
                    codeLines.Remove(zeroLine.DocLine);
                    codeLines.Add(updatedLine.DocLine, updatedLine);
                }
            }
        }

        private void insertCodeLines(int offset, int length)
        {
            DocumentLine startLine = document.GetLineByOffset(offset);
            DocumentLine endLine = document.GetLineByOffset(offset + length + 1);
            IEnumerable<CodeLine> code = getCodeLines(startLine, endLine);

            CodeLine first = code.FirstOrDefault();
            if (first == null) return;
            CodeLine prev, next;

            if (codeLines.ContainsKey(startLine)) prev = codeLines[startLine].PreviouseLine;
            else
            {
                DocumentLine tmp = startLine.PreviousLine;
                while (tmp != null && !codeLines.ContainsKey(tmp))
                    tmp = tmp.PreviousLine;
                prev = (tmp != null) ? codeLines[tmp] : null;
            }

            if (codeLines.ContainsKey(endLine)) next = codeLines[endLine].NextLine;
            else
            {
                DocumentLine tmp = startLine.NextLine;
                while (tmp != null && !codeLines.ContainsKey(tmp))
                    tmp = tmp.NextLine;
                next = (tmp != null) ? codeLines[tmp] : null;
            }

            CodeLine start = null;
            CodeLine end = null;
            foreach (var codeLine in code)
            {
                if (start == null) start = codeLine;
                end = codeLine;
                if (codeLines.ContainsKey(end.DocLine))
                    codeLines[end.DocLine] = end;
                else codeLines.Add(end.DocLine, end);
            }
            start.PreviouseLine = prev;
            end.NextLine = next;
            if (prev != null) prev.NextLine = start;
            if (next != null) next.PreviouseLine = end;
        }

        private void removeCodeLines(int offset)
        {
            DocumentLine startLine = document.GetLineByOffset(offset);
            if (codeLines.ContainsKey(startLine))
            {
                CodeLine startCdLine = codeLines[startLine];
                string startLineContent = document.GetText(startCdLine.DocLine.Offset, startCdLine.DocLine.Length);
                if (string.IsNullOrEmpty(startLineContent))
                    startCdLine = startCdLine.PreviouseLine;

                removeCodeLine(startCdLine, startLine);
            }
            else
            {
                DocumentLine tmp = startLine.PreviousLine;
                while (tmp != null && !codeLines.ContainsKey(tmp))
                    tmp = tmp.PreviousLine;
                CodeLine startCdLine = (tmp != null) ? startCdLine = codeLines[tmp] : null;

                removeCodeLine(startCdLine, startLine);
            }
        }

        private void removeCodeLine(CodeLine startCodeLine, DocumentLine startDocumentLine)
        {
            DocumentLine tmp = startDocumentLine.NextLine;
            while (tmp != null && !codeLines.ContainsKey(tmp))
                tmp = tmp.NextLine;
            CodeLine endLine = (tmp != null) ? endLine = codeLines[tmp] : null;

            if (startCodeLine != null) startCodeLine.ParseResult = null;
            if (endLine != null) endLine.ParseResult = null;

            if (startCodeLine == null)
            {
                if (endLine == null) return;
                else
                {
                    CodeLine cdTmp = endLine.PreviouseLine;
                    while (cdTmp != null)
                    {
                        codeLines.Remove(cdTmp.DocLine);
                        cdTmp = cdTmp.PreviouseLine;
                    }
                    endLine.PreviouseLine = null;
                }
            }
            else
            {

                CodeLine cdTmp = startCodeLine.NextLine;
                while (cdTmp != null && cdTmp != endLine)
                {
                    codeLines.Remove(cdTmp.DocLine);
                    cdTmp = cdTmp.NextLine;
                }
            }
            if (startCodeLine != null) startCodeLine.NextLine = endLine;
            if (endLine != null) endLine.PreviouseLine = startCodeLine;
        }

        #region Get code lines from the document
        private IEnumerable<CodeLine> getCodeLines(IEnumerable<DocumentLine> lines)
        {
            CodeLine last = null;
            foreach (var line in lines)
            {
                string lineText = document.GetText(line.Offset, line.Length).Trim();
                if (!string.IsNullOrEmpty(lineText) && !lineText.StartsWith(slCommentStart))
                {
                    CodeLine cdLine = new CodeLine(line);
                    cdLine.PreviouseLine = last;
                    if (last != null) last.NextLine = cdLine;
                    last = cdLine;
                    yield return cdLine;
                }
            }
        }
        private IEnumerable<CodeLine> getCodeLines(DocumentLine startLine, DocumentLine endLine)
        {
            IEnumerable<DocumentLine> lines = getDocumentLines();
            CodeLine last = null;
            foreach (var line in lines)
            {
                string lineText = document.GetText(line.Offset, line.Length).Trim();
                if (!string.IsNullOrEmpty(lineText) && !lineText.StartsWith(slCommentStart))
                {
                    CodeLine cdLine = new CodeLine(line);
                    cdLine.PreviouseLine = last;
                    if (last != null) last.NextLine = cdLine;
                    last = cdLine;
                    yield return cdLine;
                }
            }

            IEnumerable<DocumentLine> getDocumentLines()
            {
                if (startLine.Offset > endLine.Offset) yield break;
                DocumentLine tmp = startLine;
                yield return tmp;
                while (tmp != endLine)
                {
                    tmp = tmp.NextLine;
                    yield return tmp;
                }
            }
        }
        private bool isCodeLine(DocumentLine line)
        {
            string lineText = document.GetText(line.Offset, line.Length).Trim();
            return !string.IsNullOrEmpty(lineText) && !lineText.StartsWith(slCommentStart);
        }
        #endregion

        //public int getParseErrorCount() => getParseErrors().ToList().Count;

        //public IEnumerable<ParseError> getParseErrors()
        //    => from command in commands
        //       where command.Error != null
        //       select command.Error;

        private ParseResult parseCodeLine(CodeLine codeLine)
        {
            string codeLineTxt = codeLine.DocLine.getText(document);
            string[] words = codeLineTxt.Split(' ');
            if (!cmdRegex.ContainsKey(words[0]))
            {
                // invalid command keyword
                ParseError parseError = new ParseError(ParseError.ErrorType.InvalidKeyWord, codeLineTxt, codeLine.DocLine);
                return new ParseResult() { Error = parseError };
            }
            string regexPattern = cmdRegex[words[0]];
            Match match = Regex.Match(codeLineTxt, regexPattern);
            if (!match.Success)
            {
                // invalid Argument
                ParseError parseError = new ParseError(ParseError.ErrorType.InvalidArgument,
                                                       codeLineTxt.Substring(words[0].Length), codeLine.DocLine);
                return new ParseResult() { Error = parseError };
            }
            string annex = codeLineTxt.Substring(match.Length);
            if (!string.IsNullOrEmpty(annex))
            {
                // invalid command syntax
                ParseError parseError = new ParseError(ParseError.ErrorType.InvalidCommandSyntax,
                                                       annex, codeLine.DocLine);
                return new ParseResult() { Error = parseError };
            }
            CPUCommand.CommandKeyWord keyWord = cmdKeyWords[words[0]];
            byte argument = 0x00;
            if (keyWord.hasArgument)
            {
                bool toLarge = (words[1].isHex() && words[1].Length > 4) ||
                               (words[1].isBinary() && words[1].Length > 10);
                if (toLarge)
                {
                    // Argument to large
                    ParseError parseError = new ParseError(ParseError.ErrorType.InvalidArgumentSize,
                                                           codeLineTxt.Substring(words[0].Length + 1), codeLine.DocLine);
                    return new ParseResult() { Error = parseError };
                }
                argument = words[1].isHex() ? Convert.ToByte(words[1], 16) : Convert.ToByte(words[1], 2);
            }
            CPUCommand cmd = CPUCommand.getCommand(keyWord, argument);
            return new ParseResult() { Command = cmd };
        }

        public void parseCode()
        {
            var query = from codeLine in codeLines.Values
                        where !codeLine.isParse
                        select codeLine;

            foreach (var codeLine in query)
                codeLine.ParseResult = parseCodeLine(codeLine);
        }

        IEnumerable<CPUCommand> getCommands()
        {
            return null;
        }

        private class ParseResult
        {
            public ParseError Error;
            public CPUCommand Command;
        }

        private class CodeLine
        {
            public CodeLine PreviouseLine;
            public CodeLine NextLine;
            public DocumentLine DocLine;

            public ParseResult ParseResult;
            public bool isParse { get { return ParseResult != null; } }

            public CodeLine(DocumentLine docLine)
            {
                DocLine = docLine;
            }

            public CodeLine(DocumentLine docLine, CodeLine prev, CodeLine next)
            {
                DocLine = docLine;
                PreviouseLine = prev;
                NextLine = next;
            }
        }
    }
}