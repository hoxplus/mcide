﻿using ICSharpCode.AvalonEdit.Document;
using System;

namespace MicroCode.Compiler
{
    public class ParseError
    {
        public ErrorType ParseErrorType { get; private set; }
        public string FaultyString { get; private set; }
        public DocumentLine DocLine { get; internal set; }

        public ParseError(ErrorType type, string faultyString, DocumentLine docLine)
        {
            ParseErrorType = type;
            FaultyString = faultyString;
            DocLine = docLine;
        }

        public enum ErrorType
        {
            InvalidKeyWord,
            InvalidArgument,
            InvalidArgumentSize,
            InvalidCommandSyntax,
        }
    }
}
