﻿using ICSharpCode.AvalonEdit.Document;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroCode.Utils
{
    static class Extensions
    {
        public static bool isHex(this string source)
        {
            return source[1] == 'x' || source[1] == 'X';
        }

        public static bool isBinary(this string source)
        {
            return source[1] == 'b' || source[1] == 'B';
        }

        public static string getText(this DocumentLine line, TextDocument document)
        {
            return document.GetText(line.Offset, line.Length);
        }

        public static void ChangeKey<TKey, TValue>(this IDictionary<TKey, TValue> dic,
                                      TKey fromKey, TKey toKey)
        {
            TValue value = dic[fromKey];
            dic.Remove(fromKey);
            dic.Add(toKey, value);
        }
    }
}
