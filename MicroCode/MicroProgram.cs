﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroCode
{
    class MicroProgram
    {
        private static int jumpTableSize = 16;
        private static int constTableSize = 16;
        private static int programMemorySize = 256;
        //private static int ramSize = 32;


        private byte[] jumpTable = new byte[jumpTableSize];
        private byte[] constTable = new byte[constTableSize];
        private byte[] commandTable = new byte[programMemorySize];
    }
}
