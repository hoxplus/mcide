﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MicroCode
{
    /// <summary>
    /// 
    /// </summary>
    class CPUCommand
    {
        public CommandKeyWord KeyWord { get; private set; }
        public byte Argument { get; private set; }
        public byte MicroCode
        {
            get
            {
                return (byte)((KeyWord.Code << 4) + (Argument & 0x0F));
            }
        }

        private CPUCommand(CommandKeyWord keyWord, byte argument)
        {
            KeyWord = keyWord;
            Argument = argument;
        }

        public static CPUCommand getCommand(CommandKeyWord keyWord, byte argument)
            => new CPUCommand(keyWord, argument);

        /// <summary>
        /// 
        /// </summary>
        public class CommandKeyWord
        {
            public static readonly CommandKeyWord ADD = new CommandKeyWord(0x01, true);
            public static readonly CommandKeyWord JMP = new CommandKeyWord(0x02, true);
            public static readonly CommandKeyWord HLT = new CommandKeyWord(0x15, false);
            //...

            public byte Code { get; private set; }
            public bool hasArgument { get; private set; }

            private CommandKeyWord(byte code, bool hasArgument)
            {
                Code = code;
                this.hasArgument = hasArgument;
            }
        }
    }
}
